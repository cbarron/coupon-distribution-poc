package com.tillster.platform.service.ws;

import com.tillster.BuildVersion;
import com.tillster.platform.service.CouponDistributionService;
import com.tillster.platform.service.model.DistributionRule;
import com.tillster.platform.service.model.DistributionRuleId;
import com.tillster.platform.service.model.exceptions.DistributionRuleExistsException;
import com.tillster.platform.service.model.exceptions.NoSuchDistributionRuleException;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CouponDistributionWebServiceImpl implements CouponDistributionWebService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CouponDistributionWebServiceImpl.class);

    @Inject
    private BuildVersion buildVersion;

    @Inject
    private CouponDistributionService service;

    @Override
    public DistributionRule get(DistributionRuleId id) throws NoSuchDistributionRuleException {
        return service.getDistributionRule(id);
    }

    @Override
    public DistributionRule create(DistributionRule distributionRule) throws DistributionRuleExistsException, NoSuchDistributionRuleException {
        return service.createDistributionRule(distributionRule);
    }

    @Override
    public BuildVersion getVersion() {
        return buildVersion;
    }
}
