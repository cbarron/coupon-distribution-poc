/**
 * Converters
 *
 * Used to convert Service exceptions into the format necessary for JAX-RS
 */
package com.tillster.platform.service.ws.converters.exceptions;
