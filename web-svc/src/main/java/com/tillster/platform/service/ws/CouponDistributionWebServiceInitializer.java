package com.tillster.platform.service.ws;

import static org.springframework.web.context.ContextLoader.CONFIG_LOCATION_PARAM;
import static org.springframework.web.context.ContextLoader.CONTEXT_CLASS_PARAM;

import com.tillster.service.user.auth.AuthApplication;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.request.RequestContextListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;

/**
 * The {@link CouponDistributionWebServiceInitializer} is the JavaConfig equivalent of the {@code web.xml} file.
 *
 * @author Mirko Raner (Tillster, Inc.)
 */
@Order(Ordered.HIGHEST_PRECEDENCE)
public class CouponDistributionWebServiceInitializer implements WebApplicationInitializer {

    @Override
    public void onStartup(ServletContext context) throws ServletException {
        context.setInitParameter(CONTEXT_CLASS_PARAM, AnnotationConfigWebApplicationContext.class.getName());
        context.setInitParameter(CONFIG_LOCATION_PARAM, CouponDistributionWebServiceConfiguration.class.getName());
        context.addListener(RequestContextListener.class);
        context.addListener(ContextLoaderListener.class);
        ServletRegistration.Dynamic registration = context.addServlet("jersey-servlet", "org.glassfish.jersey.servlet.ServletContainer");
        registration.setInitParameter(
                "jersey.config.server.provider.classnames", CouponDistributionWebServiceImpl.class.getName() + ", org.glassfish.jersey.filter.LoggingFilter");
        registration.setInitParameter("jersey.config.server.provider.packages", "com.tillster.platform.service.ws.converters, org.codehaus.jackson.jaxrs");
        registration.setInitParameter("jersey.config.server.tracing.type", "OFF");
        registration.setInitParameter("jersey.config.server.tracing.threshold", "TRACE");
        registration.setInitParameter("javax.ws.rs.Application", AuthApplication.class.getName());
        registration.setLoadOnStartup(1);
        registration.addMapping("/rest/*");
    }
}
