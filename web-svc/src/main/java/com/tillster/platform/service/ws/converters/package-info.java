/**
 * Converters
 *
 * Special converters to convert from our objects to JAX-RS compatible objects
 */
package com.tillster.platform.service.ws.converters;
