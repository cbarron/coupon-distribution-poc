package com.tillster.platform.service.ws;

import com.tillster.platform.service.CouponDistributionServiceConfiguration;
import com.tillster.service.user.auth.AuthConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * The {@link CouponDistributionWebServiceConfiguration} configures the Location web service.
 *
 * @author Mirko Raner (Tillster, Inc.)
 */
@Configuration
// AuthConfiguration is imported here to support the @Authenticated annotation which will look up tickets against the User-service automatically
@Import({CouponDistributionServiceConfiguration.class, AuthConfiguration.class})
public class CouponDistributionWebServiceConfiguration {}
