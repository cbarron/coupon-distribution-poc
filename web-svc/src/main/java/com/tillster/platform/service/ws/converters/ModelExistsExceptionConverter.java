package com.tillster.platform.service.ws.converters;

import static javax.ws.rs.core.Response.Status.BAD_REQUEST;

import com.tillster.platform.service.model.exceptions.DistributionRuleExistsException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * Created by robert on 8/25/16.
 */
@Provider
public class ModelExistsExceptionConverter implements ExceptionMapper<DistributionRuleExistsException> {

    static class Error {

        private DistributionRuleExistsException modelExistsException;

        public DistributionRuleExistsException getModelExistsException() {
            return modelExistsException;
        }

        public void setModelExistsException(DistributionRuleExistsException exception) {
            this.modelExistsException = exception;
        }
    }

    @Override
    public Response toResponse(DistributionRuleExistsException exception) {

        Error entity = new Error();
        entity.setModelExistsException(exception);
        return Response.status(BAD_REQUEST).entity(entity).build();
    }
}
