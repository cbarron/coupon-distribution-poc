package com.tillster.platform.service.ws.converters;

import static javax.ws.rs.core.Response.Status.NOT_FOUND;

import com.tillster.platform.service.model.exceptions.NoSuchDistributionRuleException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * Created by robert on 8/25/16.
 */
@Provider
public class NoSuchModelExceptionConverter implements ExceptionMapper<NoSuchDistributionRuleException> {

    static class Error {

        private NoSuchDistributionRuleException noSuchModelException;

        public NoSuchDistributionRuleException getNoSuchModelException() {
            return noSuchModelException;
        }

        public void setNoSuchModelException(NoSuchDistributionRuleException noSuchModelException) {
            this.noSuchModelException = noSuchModelException;
        }
    }

    @Override
    public Response toResponse(NoSuchDistributionRuleException noSuchModelException) {

        Error entity = new Error();
        entity.setNoSuchModelException(noSuchModelException);
        return Response.status(NOT_FOUND).entity(entity).build();
    }
}
