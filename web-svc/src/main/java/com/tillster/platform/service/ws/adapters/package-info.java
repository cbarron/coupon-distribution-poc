/**
 * Adapters
 *
 * Used to help with converting JSON from a preferred custom style into map objects
 *
 * See Location-Service fro more examples
 *
 */
package com.tillster.platform.service.ws.adapters;
