package com.tillster.platform.service.ws.integration.test;

import static javax.ws.rs.core.HttpHeaders.ACCEPT_ENCODING;
import static javax.ws.rs.core.HttpHeaders.CONTENT_TYPE;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import com.tillster.QuickTest;
import com.tillster.SlowTest;
import com.tillster.platform.service.ws.CouponDistributionWebService;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Callable;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.ws.Holder;
import javax.xml.ws.http.HTTPException;
import org.apache.catalina.Context;
import org.apache.catalina.LifecycleException;
import org.apache.catalina.core.StandardContext;
import org.apache.catalina.startup.Tomcat;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpMessage;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The {@link WebServiceIntegrationTest} is an integration test that verifies the basic
 * functionality of the {@link CouponDistributionWebService}. It installs the WAR file on an embedded
 * {@link Tomcat} server and then sends HTTP requests to that server.
 *
 * By directly running the {@link #main(String...)} method, this class can also be used to
 * quickly spin up a server for local manual tests.
 *
 * @author Mirko Raner (Tillster, Inc.)
 */
@Category({QuickTest.class, SlowTest.class})
public class WebServiceIntegrationTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(WebServiceIntegrationTest.class);

    private static final String MOCK_AUTH = "user-service-web";
    private static final String VALID_TICKET = "12345678-9ABC-DEF0-1234-567890ABCDEF";
    private static final String INVALID_TICKET = "00000000-0000-0000-0000-000000000000";
    private static final Pattern ticketPattern = Pattern.compile("\\{\"ticketId\":\"([A-Z0-9-]+)\"\\}");

    private static final String MODEL_NAME = "thailand";
    private static final String MODEL = "{\"name\":\"" + MODEL_NAME + "\"}";

    private static Map<String, String> ticketToUser = new HashMap<>();

    static {
        ticketToUser.put(VALID_TICKET, "user1");
    }

    // TODO: change name
    private static final String SERVICE_NAME = "coupon-distribution";

    private static boolean mockExternalServices = true;
    private static int port = 8181;
    private static String baseUrl;
    private static Tomcat server;
    private static JsonParser parser = new JsonParser();
    private CloseableHttpClient httpClient;

    /**
     * Starts an embedded Tomcat server for purposes of interactive testing.
     * @param arguments the command-line arguments (not evaluated)
     * @throws Exception if the embedded Tomcat server could not be started
     */
    public static void main(String... arguments) throws Exception {

        mockExternalServices = false;
        port = 8282;
        startTomcat();
        LOGGER.debug("Tomcat started on port " + port);
        server.getServer().await();
    }

    @BeforeClass
    public static void startTomcat() throws Exception {
        try (InputStream testProperties = resourceStream("coupon-distribution-service-test.properties")) {
            Properties properties = new Properties();
            properties.load(testProperties);
            properties.forEach((key, value) -> System.setProperty((String) key, (String) value));
            System.setProperty("auth.url", "http://localhost:" + port + "/" + MOCK_AUTH);
        }
        while (port < 65536 && server == null) {
            try {
                if (mockExternalServices) {
                    LOGGER.debug("Mocking external services...");
                    //
                    // Add mock service handlers here...
                }
                server = new Tomcat();
                server.setPort(port);
                String tmpdir = tmpdir();
                LOGGER.debug("tmpdir=" + tmpdir);
                server.setBaseDir(tmpdir);
                File webapps = new File(tmpdir, "webapps");
                webapps.delete();
                webapps.mkdir();
                File war = new File("target/" + SERVICE_NAME + ".war");
                assertTrue(war.exists());
                StandardContext context = (StandardContext) server.addWebapp("/" + SERVICE_NAME, war.getAbsolutePath());
                context.setIgnoreAnnotations(false);

                // Register our mock user service
                String ctxBase = "/" + MOCK_AUTH;
                Servlet userServlet = getAuthServlet();
                String userServletName = userServlet.getClass().getSimpleName();
                Context userServletContext = server.addContext(ctxBase, new File(System.getProperty("java.io.tmpdir")).getAbsolutePath());
                URL userServletUrl = new URL("http://localhost:" + port + ctxBase);
                Tomcat.addServlet(userServletContext, userServletName, userServlet);
                userServletContext.addServletMapping("/*", userServletName);
                System.setProperty("auth.url", userServletUrl.toString());

                server.start();
                baseUrl = "http://localhost:" + port + "/" + SERVICE_NAME;
                break;
            } catch (@SuppressWarnings("unused") LifecycleException portAlreadyInUse) {
                LOGGER.debug("Port in use, trying next one...");
                server = null;
                port++;
            }
        }
    }

    @AfterClass
    public static void stopTomcat() throws Exception {
        server.stop();
    }

    @Before
    public void createHttpClient() {
        httpClient = HttpClients.createDefault();
    }

    /**
     * Add functions to clean up the system between tests
     * @throws Exception
     */
    @After
    public void closeHttpClient() throws Exception {
        //url("/rest/changeme/models/" + MODEL_NAME).ticket(VALID_TICKET).delete();
        httpClient.close();
    }

    @Test
    public void version() throws Exception {
        JsonElement result = url("/rest/rules/version").get();
        assertNotNull(result);
        assertEquals("test123", result.getAsJsonObject().get("version").getAsString());
    }

    private static InputStream resourceStream(String path) {
        ClassLoader loader = WebServiceIntegrationTest.class.getClassLoader();
        return loader.getResourceAsStream(path);
    }

    private static String tmpdir() {
        File tmpdir = new File(System.getProperty("java.io.tmpdir"), "embeddedTomcat");
        deleteFolder(tmpdir);
        tmpdir.mkdirs();
        return tmpdir.getAbsolutePath();
    }

    private static void deleteFolder(File folder) {
        File[] files = folder.listFiles();
        if (files != null) { //some JVMs return null for empty dirs
            for (File f : files) {
                if (f.isDirectory()) {
                    deleteFolder(f);
                } else {
                    f.delete();
                }
            }
        }
        folder.delete();
    }

    private void assertStatus(int status, Callable<JsonElement> request) throws Exception {
        Holder<HTTPException> httpException = new Holder<>();
        try {
            request.call();
        } catch (HTTPException exception) {
            httpException.value = exception;
        }
        assertEquals(status, httpException.value.getStatusCode());
    }

    /**
     * A mock auth servlet to allow the use of @Authenticated in integration tests
     * @return
     */
    private static HttpServlet getAuthServlet() {
        return new HttpServlet() {

            @Override
            protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
                if (request.getRequestURI().startsWith("/" + MOCK_AUTH)) {
                    int length = request.getContentLength();
                    byte[] content = new byte[length];
                    DataInputStream stream = new DataInputStream(request.getInputStream());
                    stream.readFully(content);
                    String messageBody = new String(content);
                    Matcher matcher = ticketPattern.matcher(messageBody);
                    if (matcher.matches() && ticketToUser.containsKey(matcher.group(1))) {
                        String user = ticketToUser.get(matcher.group(1));
                        response.setHeader("Content-Type", "application/json");
                        response.getWriter()
                                .println(
                                        "{\"id\":\"d392fddf-5809-42f3-b81d-85af97b6da34\",\"userId\":\""
                                                + user
                                                + "\",\"expiryTime\":1451522650456,\"tenant\":\"br\"}");
                        response.setStatus(200);
                    } else {
                        response.setStatus(404);
                    }
                } else {
                    response.setStatus(500);
                }
            }
        };
    }

    private RequestBuilder url(String url) {
        return new RequestBuilder(url);
    }

    // Maybe move to Apache or Jersey RequestBuilder if this gets any more complex...
    private class RequestBuilder {
        private String url;
        private InputStream body;
        private String ticket;
        private String contentType = APPLICATION_JSON;
        private Predicate<Integer> failureStatus = status -> status >= 300;

        RequestBuilder(String url) {
            this.url = url;
        }

        RequestBuilder body(String string) {
            body = new ByteArrayInputStream(string.getBytes());
            return this;
        }

        RequestBuilder body(InputStream stream) {
            body = stream;
            return this;
        }

        RequestBuilder contentType(String type) {
            contentType = type;
            return this;
        }

        RequestBuilder ticket(String string) {
            ticket = string;
            return this;
        }

        JsonElement post() throws IOException {
            return execute(withBody(body, withTicket(new HttpPost(baseUrl + url))));
        }

        JsonElement put() throws IOException {
            return execute(withBody(body, withTicket(new HttpPut(baseUrl + url))));
        }

        JsonElement get() throws IOException {
            return execute(withTicket(new HttpGet(baseUrl + url)));
        }

        int delete() throws IOException {
            try (CloseableHttpResponse response = httpClient.execute(withTicket(new HttpDelete(baseUrl + url)))) {
                return response.getStatusLine().getStatusCode();
            }
        }

        private JsonElement execute(HttpUriRequest request) throws IOException {
            try (CloseableHttpResponse response = httpClient.execute(request)) {
                return parse(response);
            }
        }

        private JsonElement parse(HttpResponse response) throws IOException {
            int statusCode = response.getStatusLine().getStatusCode();
            if (failureStatus.test(statusCode)) {
                throw new HTTPException(statusCode);
            }
            if (null != response.getEntity()) {
                String entity = EntityUtils.toString(response.getEntity());
                try {
                    return parser.parse(entity);
                } catch (JsonParseException syntaxError) {
                    throw new JsonParseException(entity, syntaxError.getCause());
                }
            } else {
                return null;
            }
        }

        private <T extends HttpMessage> T withTicket(T request) {
            if (ticket != null) {
                request.addHeader("ticket", ticket);
            }
            return request;
        }

        private <T extends HttpEntityEnclosingRequest> T withBody(InputStream entity, T request) {
            request.addHeader(CONTENT_TYPE, contentType);
            request.addHeader(ACCEPT_ENCODING, "application/json");
            request.setEntity(new InputStreamEntity(entity));
            return request;
        }
    }
}
