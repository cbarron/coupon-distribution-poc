package com.tillster.platform.service;

import com.tillster.platform.service.model.DistributionRule;
import com.tillster.platform.service.model.DistributionRuleId;
import com.tillster.platform.service.model.exceptions.DistributionRuleExistsException;
import com.tillster.platform.service.model.exceptions.NoSuchDistributionRuleException;

/**
 * Definition of the service
 */
public interface CouponDistributionService {

    DistributionRule createDistributionRule(DistributionRule rule) throws DistributionRuleExistsException, NoSuchDistributionRuleException;

    DistributionRule updateDistributionRule(DistributionRule rule);

    DistributionRule getDistributionRule(DistributionRuleId id) throws NoSuchDistributionRuleException;
}
