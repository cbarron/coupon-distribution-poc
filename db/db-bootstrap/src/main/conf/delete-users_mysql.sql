DROP USER '${database.user}'@'localhost';
DROP USER '${database.user}'@'%';
DROP USER '${database.schema.user}'@'localhost';
DROP USER '${database.schema.user}'@'%';
flush privileges;
