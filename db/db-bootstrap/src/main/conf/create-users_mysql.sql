/*
 * http://dev.mysql.com/doc/refman/5.5/en/adding-users.html excerpt:
 * It is necessary to have both accounts to be able to connect from anywhere.
 * Without the localhost account, the anonymous-user account for localhost that is created
 * would take precedence when that user connects from the local host.
 * As a result, that user would be treated as an anonymous user. */

CREATE USER '${database.schema.user}'@'localhost' IDENTIFIED BY '${database.schema.password}';
GRANT ALL PRIVILEGES ON `${database.name}`.* TO '${database.schema.user}'@'localhost';
CREATE USER '${database.schema.user}'@'%' IDENTIFIED BY '${database.schema.password}';
GRANT ALL PRIVILEGES ON `${database.name}`.* TO '${database.schema.user}'@'%';

CREATE USER '${database.user}'@'localhost' IDENTIFIED BY '${database.password}';
GRANT SELECT, INSERT, UPDATE, DELETE, EXECUTE, CREATE TEMPORARY TABLES on `${database.name}`.* TO '${database.user}'@'localhost';
CREATE USER '${database.user}'@'%' IDENTIFIED BY '${database.password}';
GRANT SELECT, INSERT, UPDATE, DELETE, EXECUTE, CREATE TEMPORARY TABLES  on `${database.name}`.* TO '${database.user}'@'%';
