package com.tillster;

/**
 * Buildversion.java
 *
 * Allows the buildversion to be returned by JAX-RS in JSON format
 * This is used by the default deployment scripts
 */
public class BuildVersion {

    private String version;

    public BuildVersion() {
        super();
    }

    public BuildVersion(String version) {
        this.version = version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getVersion() {
        return version;
    }
}
