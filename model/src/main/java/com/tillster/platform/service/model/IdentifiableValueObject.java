package com.tillster.platform.service.model;

import static org.apache.commons.lang3.builder.EqualsBuilder.reflectionEquals;
import static org.apache.commons.lang3.builder.HashCodeBuilder.reflectionHashCode;
import static org.apache.commons.lang3.builder.ToStringBuilder.reflectionToString;

import javax.xml.bind.annotation.XmlTransient;

/**
 * The {@link IdentifiableValueObject} class is an abstract base class for all model entities that have a string ID
 * and implement the Value Object pattern (i.e., equality is not based on identity). However,
 * this does not imply that subclasses are automatically immutable..
 *
 * @author Mirko Raner (Tillster, Inc.)
 */
@XmlTransient
public abstract class IdentifiableValueObject {

    private String id;

    protected IdentifiableValueObject() {
        super();
    }

    protected IdentifiableValueObject(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object other) {
        return reflectionEquals(this, other);
    }

    @Override
    public int hashCode() {
        return reflectionHashCode(this);
    }

    @Override
    public String toString() {
        return reflectionToString(this);
    }
}
