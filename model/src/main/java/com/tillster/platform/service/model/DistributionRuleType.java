package com.tillster.platform.service.model;

public enum DistributionRuleType {
    EVENT,
    IMMEDIATE,
    SCHEDULED,
    BIRTHDAY_ANNIVERSARY,
    ACCOUNT_ANNIVERSARY;
}
