package com.tillster.platform.service.model.exceptions;

/**
 * Created by robert on 8/25/16.
 */
public class NoSuchDistributionRuleException extends Exception {

    public NoSuchDistributionRuleException() {
        super();
    }
}
