package com.tillster.platform.service.model.exceptions;

/**
 * Created by robert on 8/27/16.
 */
public class DistributionRuleExistsException extends Exception {

    public DistributionRuleExistsException() {
        super();
    }
}
