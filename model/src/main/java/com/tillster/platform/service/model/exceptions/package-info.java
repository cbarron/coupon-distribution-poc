/**
 * Definitions of any Exceptions that are used in the service
 *
 * The use of custom exceptions allows us to clearly identify exception cases
 */
package com.tillster.platform.service.model.exceptions;
