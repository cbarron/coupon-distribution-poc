package com.tillster.platform.service.model;

import java.time.Instant;
import java.util.Map;

public class EventNotification {

    private Instant timestamp;

    private EventNotificationType type;

    private String tenant;

    private Map<String, String> data;

    public Instant getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Instant timestamp) {
        this.timestamp = timestamp;
    }

    public EventNotificationType getType() {
        return type;
    }

    public void setType(EventNotificationType type) {
        this.type = type;
    }

    public String getTenant() {
        return tenant;
    }

    public void setTenant(String tenant) {
        this.tenant = tenant;
    }

    public Map<String, String> getData() {
        return data;
    }

    public void setData(Map<String, String> data) {
        this.data = data;
    }
}
