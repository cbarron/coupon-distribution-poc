package com.tillster.platform.service.model;

public enum EventNotificationType {
    NEW_USER_REGISTRATION("new-user-registration");

    final String queue;

    EventNotificationType(String queue) {
        this.queue = queue;
    }

    public String getQueue() {
        return this.queue;
    }
}
