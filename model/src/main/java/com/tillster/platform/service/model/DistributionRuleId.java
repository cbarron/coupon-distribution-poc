package com.tillster.platform.service.model;

import static org.apache.commons.lang3.builder.EqualsBuilder.reflectionEquals;
import static org.apache.commons.lang3.builder.HashCodeBuilder.reflectionHashCode;
import static org.apache.commons.lang3.builder.ToStringBuilder.reflectionToString;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.NONE)
public class DistributionRuleId {

    @XmlAttribute
    private final String distributionRuleId;

    @SuppressWarnings("unused")
    private DistributionRuleId() {
        this(null);
    }

    public DistributionRuleId(final String distributionRuleId) {
        this.distributionRuleId = distributionRuleId;
    }

    public String getDistributionRuleId() {
        return distributionRuleId;
    }

    @Override
    public boolean equals(Object other) {
        return reflectionEquals(this, other);
    }

    @Override
    public int hashCode() {
        return reflectionHashCode(this);
    }

    @Override
    public String toString() {
        return reflectionToString(this);
    }
}
