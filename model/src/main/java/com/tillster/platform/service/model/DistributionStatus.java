package com.tillster.platform.service.model;

public enum DistributionStatus {
    NEW,
    QUEUED,
    PROCESSED,
    ERROR,
    RUNNING;
}
