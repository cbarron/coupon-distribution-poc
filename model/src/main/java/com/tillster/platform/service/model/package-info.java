/**
 * Internal Domain model objects
 *
 * Model.java is provided as an example domain object.
 *
 * Clone and extend.
 *
 * Maps is a special class containing extensions of map.  These are needed in order to prevent the need for hugely verbose definitions of maps in JSON
 */
package com.tillster.platform.service.model;
