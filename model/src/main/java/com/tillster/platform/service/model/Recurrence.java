package com.tillster.platform.service.model;

public enum Recurrence {
    DAILY,
    MONTHLY;
}
