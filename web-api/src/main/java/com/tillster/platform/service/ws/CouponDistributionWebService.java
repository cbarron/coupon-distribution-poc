package com.tillster.platform.service.ws;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

import com.tillster.BuildVersion;
import com.tillster.platform.service.model.DistributionRule;
import com.tillster.platform.service.model.DistributionRuleId;
import com.tillster.platform.service.model.exceptions.DistributionRuleExistsException;
import com.tillster.platform.service.model.exceptions.NoSuchDistributionRuleException;
import com.tillster.service.user.auth.annotations.Authenticated;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

@Path("/rules")
@Consumes(APPLICATION_JSON)
@Produces(APPLICATION_JSON)
public interface CouponDistributionWebService {

    @GET
    @Path("/{rule-id}")
    DistributionRule get(@PathParam("rule-id") DistributionRuleId id) throws NoSuchDistributionRuleException;

    @Authenticated
    @POST
    DistributionRule create(DistributionRule distributionRule) throws DistributionRuleExistsException, NoSuchDistributionRuleException;

    /**
     * Returns the version of the service.
     *
     * This is defined at buildtime and comes from the mvn project.version
     *
     * @return
     */
    @GET
    @Path("/version")
    BuildVersion getVersion();
}
