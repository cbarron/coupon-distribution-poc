package com.tillster.platform.service;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.util.ISO8601DateFormat;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.ServerAddress;
import com.tillster.BuildVersion;
import com.tillster.platform.service.datamodel.DistributionRuleRepository;
import com.tillster.platform.service.datamodel.UserEntity;
import com.tillster.platform.service.datamodel.jobs.AccountAnniversaryJob;
import com.tillster.platform.service.datamodel.jobs.TenantUserJob;
import java.io.IOException;
import java.util.Properties;
import javax.inject.Inject;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.PropertiesFactoryBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.config.EnableMongoAuditing;
import org.springframework.data.mongodb.core.MongoExceptionTranslator;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.client.RestTemplate;

/**
 * {@link CouponDistributionServiceConfiguration} is an annotation-based replacement for Spring's XML configuration files.
 */
@Configuration
@EnableTransactionManagement
@EnableMongoRepositories(basePackageClasses = DistributionRuleRepository.class)
@EnableMongoAuditing
@EnableJpaRepositories(
    basePackageClasses = CouponDistributionServiceConfiguration.class,
    entityManagerFactoryRef = "userEntityManagerFactory",
    transactionManagerRef = "userTransactionManager"
)
@ComponentScan(basePackageClasses = CouponDistributionServiceConfiguration.class)
@PropertySource("classpath:coupon-distribution-service.properties")
@PropertySource(value = "file:${catalina.home}/conf/coupon-distribution-service.properties", ignoreResourceNotFound = true)
public class CouponDistributionServiceConfiguration extends AbstractMongoConfiguration {

    private static final Logger LOGGER = LoggerFactory.getLogger(CouponDistributionServiceConfiguration.class);

    @Value("${distribution.prop.project.version}")
    private String distributionVersion;

    @Value("${distribution.mongo.database.name}")
    private String mongoDatabaseName;

    @Value("${distribution.mongo.database.host}")
    private String mongoDatabaseHost;

    @Value("${distribution.mongo.database.port}")
    private int mongoDatabasePort;

    @Inject
    private ApplicationContext applicationContext;

    @Override
    protected String getDatabaseName() {
        return mongoDatabaseName;
    }

    @Bean
    public MongoExceptionTranslator exceptionTranslator() {
        return new MongoExceptionTranslator();
    }

    @Override
    public Mongo mongo() throws Exception {
        return new MongoClient(new ServerAddress(mongoDatabaseHost, mongoDatabasePort), mongoClientOptions());
    }

    @Bean
    public MongoClientOptions mongoClientOptions() {
        // override to more reasonable connection timeout (default is 10 seconds)
        return MongoClientOptions.builder().connectTimeout(1500).connectionsPerHost(8).maxWaitTime(1500).socketKeepAlive(true).build();
    }

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertySources() {
        return new PropertySourcesPlaceholderConfigurer();
    }

    @Bean
    public JpaVendorAdapter usersJpaVendorAdapter(@Value("${database.type}") String type, @Value("${database.generateDDL}") Boolean generateDDL) {
        HibernateJpaVendorAdapter hibernateJpaVendorAdapter = new HibernateJpaVendorAdapter();
        hibernateJpaVendorAdapter.setShowSql(false);
        hibernateJpaVendorAdapter.setGenerateDdl(generateDDL);
        hibernateJpaVendorAdapter.setDatabase(Database.valueOf(type));
        return hibernateJpaVendorAdapter;
    }

    @Bean
    public DataSource usersDataSource(
            @Value("${user.database.url}") String url,
            @Value("${user.database.username}") String username,
            @Value("${user.database.password}") String password,
            @Value("${user.database.driver}") String driver) {
        org.apache.tomcat.jdbc.pool.DataSource dataSource = new org.apache.tomcat.jdbc.pool.DataSource();
        dataSource.setDriverClassName(driver);
        dataSource.setUrl(url);
        dataSource.setUsername(username);
        dataSource.setPassword(password);
        dataSource.setTestOnBorrow(true);
        dataSource.setValidationQuery("SELECT 1");
        return dataSource;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean userEntityManagerFactory(DataSource usersDataSource, JpaVendorAdapter usersJpaVendorAdapter) {
        LocalContainerEntityManagerFactoryBean localContainerEntityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
        localContainerEntityManagerFactoryBean.setDataSource(usersDataSource);
        localContainerEntityManagerFactoryBean.setJpaVendorAdapter(usersJpaVendorAdapter);
        localContainerEntityManagerFactoryBean.setPackagesToScan(UserEntity.class.getPackage().getName());
        return localContainerEntityManagerFactoryBean;
    }

    @Bean
    public JpaTransactionManager userTransactionManager(EntityManagerFactory userEntityManagerFactory) {
        return new JpaTransactionManager(userEntityManagerFactory);
    }

    @Bean
    public CouponDistributionService service() {
        return new CouponDistributionServiceImpl();
    }

    @Bean
    public SchedulerFactoryBean quartzScheduler() {
        SchedulerFactoryBean quartzScheduler = new SchedulerFactoryBean();

        quartzScheduler.setOverwriteExistingJobs(true);
        quartzScheduler.setSchedulerName("coupon-distribution-quartz-scheduler");

        // custom job factory of spring with DI support for @Autowired!
        AutowiringSpringBeanJobFactory jobFactory = new AutowiringSpringBeanJobFactory();
        jobFactory.setApplicationContext(applicationContext);
        quartzScheduler.setJobFactory(jobFactory);

        quartzScheduler.setQuartzProperties(quartzProperties());

        return quartzScheduler;
    }

    @Bean
    public Properties quartzProperties() {
        PropertiesFactoryBean propertiesFactoryBean = new PropertiesFactoryBean();
        propertiesFactoryBean.setLocation(new ClassPathResource("/quartz.properties"));
        Properties properties = null;
        try {
            propertiesFactoryBean.afterPropertiesSet();
            properties = propertiesFactoryBean.getObject();

        } catch (IOException e) {
            LOGGER.warn("Cannot load quartz.properties.");
        }

        return properties;
    }

    @Bean("accountAnniversaryJobFactory")
    public JobDetailFactoryBean accountAnniversaryJobDetail() {
        JobDetailFactoryBean jobDetailFactory = new JobDetailFactoryBean();
        jobDetailFactory.setJobClass(AccountAnniversaryJob.class);
        // jobDetailFactory.setDescription("I.nvoke Sample Job service..");
        jobDetailFactory.setDurability(true);
        jobDetailFactory.setApplicationContext(applicationContext);
        return jobDetailFactory;
    }

    @Bean
    @Primary
    public ObjectMapper objectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        objectMapper.setDateFormat(new ISO8601DateFormat());
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
        objectMapper.registerModule(new ParameterNamesModule()).registerModule(new Jdk8Module()).registerModule(new JavaTimeModule());
        return objectMapper;
    }

    @Bean
    public MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter() {
        MappingJackson2HttpMessageConverter jsonConverter = new MappingJackson2HttpMessageConverter();
        jsonConverter.setObjectMapper(objectMapper());
        return jsonConverter;
    }

    @Bean
    public RestTemplate restTemplate() {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(0, mappingJackson2HttpMessageConverter());
        return restTemplate;
    }

    @Bean
    public BuildVersion distributionBuildVersion() {
        return new BuildVersion(distributionVersion);
    }
}
