package com.tillster.platform.service.datamodel;

import com.tillster.platform.service.model.Channel;
import com.tillster.platform.service.model.DistributionRuleId;
import com.tillster.platform.service.model.DistributionRuleType;
import com.tillster.platform.service.model.DistributionStatus;
import com.tillster.platform.service.model.EventNotificationType;
import com.tillster.platform.service.model.Recurrence;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "distribution_rule")
public class DistributionRuleDocument {

    @Id
    private String id;

    private String name;

    private boolean enabled;

    @Indexed
    private String tenant;

    private Date distributionTime;

    private Channel channel;

    private DistributionRuleType type;

    @Indexed
    private EventNotificationType eventTrigger;

    private List<String> couponIds;

    private List<String> userIds;

    private int dateOffset;

    private DistributionStatus status;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getTenant() {
        return tenant;
    }

    public void setTenant(String tenant) {
        this.tenant = tenant;
    }

    public Date getDistributionTime() {
        return distributionTime;
    }

    public void setDistributionTime(Date distributionTime) {
        this.distributionTime = distributionTime;
    }

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    public DistributionRuleType getType() {
        return type;
    }

    public void setType(DistributionRuleType type) {
        this.type = type;
    }

    public EventNotificationType getEventTrigger() {
        return eventTrigger;
    }

    public void setEventTrigger(EventNotificationType eventTrigger) {
        this.eventTrigger = eventTrigger;
    }

    public List<String> getCouponIds() {
        return couponIds;
    }

    public void setCouponIds(List<String> couponIds) {
        this.couponIds = couponIds;
    }

    public List<String> getUserIds() {
        return userIds;
    }

    public void setUserIds(List<String> userIds) {
        this.userIds = userIds;
    }

    public int getDateOffset() {
        return dateOffset;
    }

    public void setDateOffset(int dateOffset) {
        this.dateOffset = dateOffset;
    }

    public DistributionStatus getStatus() {
        return status;
    }

    public void setStatus(DistributionStatus status) {
        this.status = status;
    }
}
