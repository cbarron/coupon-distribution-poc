package com.tillster.platform.service;

import static org.quartz.JobBuilder.newJob;
import static org.quartz.SimpleScheduleBuilder.simpleSchedule;

import com.tillster.platform.service.datamodel.DistributionRuleDocument;
import com.tillster.platform.service.datamodel.DistributionRuleRepository;
import com.tillster.platform.service.datamodel.converters.DistributionRuleConverter;
import com.tillster.platform.service.datamodel.jobs.AccountAnniversaryJob;
import com.tillster.platform.service.datamodel.jobs.TenantUserJob;
import com.tillster.platform.service.model.DistributionRule;
import com.tillster.platform.service.model.DistributionStatus;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Inject;
import javax.inject.Named;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;
import org.springframework.stereotype.Service;

@Service
public class DistributionJobScheduler {

    @Inject
    private DistributionRuleRepository distributionRuleRepository;

    @Inject
    @Named("accountAnniversaryJobFactory")
    private JobDetailFactoryBean accountAnniversaryJobDetailFactory;

    @Inject
    private SchedulerFactoryBean schedulerFactoryBean;

    @Inject
    private DistributionRuleConverter converter;

    public void scheduleDistributionJob(final DistributionRule distributionRule) throws SchedulerException {

        Scheduler scheduler = schedulerFactoryBean.getScheduler();

        JobDetail job = null;
        Trigger trigger = null;
        switch (distributionRule.getType()) {
            case SCHEDULED:
                job = newJob(TenantUserJob.class).withIdentity(new JobKey(distributionRule.getId().getDistributionRuleId())).build();
                job.getJobDataMap().put(DistributionRule.class.toString(), distributionRule);

                if (distributionRule.getDistributionTime().toInstant().isBefore(Instant.now())) {
                    throw new IllegalArgumentException("Scheduled date is in the past.");
                }

                Date startTime = Date.from(distributionRule.getDistributionTime().toInstant());
                trigger = TriggerBuilder.newTrigger().withIdentity(distributionRule.getId().getDistributionRuleId()).forJob(job).startAt(startTime).build();
                break;
            case ACCOUNT_ANNIVERSARY:
                job = accountAnniversaryJobDetailFactory.getObject();

                Instant startInstant = Instant.now();
                if (distributionRule.getDistributionTime() != null) {
                    startInstant = distributionRule.getDistributionTime().toInstant();
                }

                Date startDate = Date.from(startInstant);

                trigger =
                        TriggerBuilder.newTrigger()
                                .withIdentity(distributionRule.getId().getDistributionRuleId())
                                .forJob(job)
                                .startAt(startDate)
                                .withSchedule(simpleSchedule().withIntervalInSeconds(60).withRepeatCount(2))
                                .build();

                job.getJobDataMap().put(DistributionRule.class.toString(), distributionRule);
                break;

            case BIRTHDAY_ANNIVERSARY:
                break;
        }

        if (job != null && trigger != null) {
            distributionRule.setStatus(DistributionStatus.QUEUED);
            distributionRuleRepository.save(converter.convert(distributionRule));
            scheduler.scheduleJob(job, trigger);
        }
    }

    @PostConstruct
    public void postConstruct() throws SchedulerException {
        List<DistributionRuleDocument> distributionRules = distributionRuleRepository.findAll();
        distributionRules
                .stream()
                .filter(document -> document.getStatus() != DistributionStatus.PROCESSED && document.getStatus() != DistributionStatus.ERROR)
                .map(document -> converter.convert(document))
                .forEach(
                        rule -> {
                            System.out.println("Starting job: " + rule.getName() + ", id: " + rule.getId());
                            try {
                                scheduleDistributionJob(rule);
                            } catch (SchedulerException e) {
                                e.printStackTrace();
                            }
                        });

        Scheduler scheduler = schedulerFactoryBean.getScheduler();
        scheduler.start();
    }

    @PreDestroy
    public void preDestroy() throws SchedulerException {
        schedulerFactoryBean.stop();
        schedulerFactoryBean.destroy();
    }
}
