package com.tillster.platform.service.datamodel.converters;

import com.tillster.platform.service.datamodel.DistributionRuleDocument;
import com.tillster.platform.service.model.DistributionRule;
import com.tillster.platform.service.model.DistributionRuleId;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

@Component
public class DistributionRuleConverter {

    public DistributionRuleDocument convert(final DistributionRule rule) {
        DistributionRuleDocument document = new DistributionRuleDocument();
        if (rule.getId() != null) {
            document.setId(rule.getId().getDistributionRuleId());
        }
        document.setChannel(rule.getChannel());
        document.setCouponIds(rule.getCouponIds());
        document.setEnabled(rule.isEnabled());
        document.setEventTrigger(rule.getEventTrigger());
        document.setName(rule.getName());
        document.setDistributionTime(rule.getDistributionTime());
        document.setTenant(rule.getTenant());
        document.setType(rule.getType());
        document.setUserIds(rule.getUserIds());
        document.setEventTrigger(rule.getEventTrigger());
        document.setStatus(rule.getStatus());
        return document;
    }

    public DistributionRule convert(final DistributionRuleDocument document) {
        DistributionRule rule = new DistributionRule();
        if (StringUtils.isNotEmpty(document.getId())) {
            rule.setId(new DistributionRuleId(document.getId()));
        }
        rule.setChannel(document.getChannel());
        rule.setCouponIds(document.getCouponIds());
        rule.setDateOffset(document.getDateOffset());
        rule.setEnabled(document.isEnabled());
        rule.setEventTrigger(document.getEventTrigger());
        rule.setName(document.getName());
        rule.setType(document.getType());
        rule.setDistributionTime(document.getDistributionTime());
        rule.setUserIds(document.getUserIds());
        rule.setTenant(document.getTenant());
        rule.setStatus(document.getStatus());
        return rule;
    }
}
