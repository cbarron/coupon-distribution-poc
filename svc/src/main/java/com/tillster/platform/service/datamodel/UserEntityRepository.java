package com.tillster.platform.service.datamodel;

import com.tillster.platform.service.datamodel.UserEntity;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserEntityRepository extends JpaRepository<UserEntity, String> {

    @Query(
        value =
                "SELECT u FROM UserEntity u WHERE u.tenantName = :tenant AND YEAR(u.createDate) < :currentYear AND MONTH(u.createDate) = :month AND DAY(u.createDate) = :day"
    )
    List<UserEntity> selectUsersByCreatedDateAnniversary(
            @Param("tenant") String tenant, @Param("currentYear") int currentYear, @Param("month") int month, @Param("day") int day);

    List<UserEntity> findByTenantName(String tenant);

    List<UserEntity> findByIdIn(List<String> userIds);
}
