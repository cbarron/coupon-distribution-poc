package com.tillster.platform.service.datamodel;

import static org.apache.commons.lang3.builder.EqualsBuilder.reflectionEquals;
import static org.apache.commons.lang3.builder.HashCodeBuilder.reflectionHashCode;

import javax.persistence.MappedSuperclass;

/**
 * An {@link IdentifiableValueObject} is an {@link IdentifiableEntity} that also loosely
 * implements the Value Object pattern (i.e., equality is not based on identity). However,
 * this does not imply that subclasses are automatically immutable.
 * This class is the JPA equivalent of {@link com.tillster.platform.service.model.IdentifiableValueObject}
 * in the {@code model} package.
 *
 * @author Mirko Raner (Tillster, Inc.)
 */
@MappedSuperclass
public abstract class IdentifiableValueObject extends IdentifiableEntity {

    @Override
    public boolean equals(Object other) {
        return reflectionEquals(this, other);
    }

    @Override
    public int hashCode() {
        return reflectionHashCode(this);
    }
}
