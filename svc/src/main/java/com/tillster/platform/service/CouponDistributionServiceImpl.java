package com.tillster.platform.service;

import com.tillster.platform.service.datamodel.DistributionRuleDocument;
import com.tillster.platform.service.datamodel.DistributionRuleRepository;
import com.tillster.platform.service.datamodel.converters.DistributionRuleConverter;
import com.tillster.platform.service.model.DistributionRule;
import com.tillster.platform.service.model.DistributionRuleId;
import com.tillster.platform.service.model.DistributionStatus;
import com.tillster.platform.service.model.exceptions.DistributionRuleExistsException;
import com.tillster.platform.service.model.exceptions.NoSuchDistributionRuleException;
import javax.inject.Inject;
import org.quartz.SchedulerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * JpaServiceImpl
 *
 * An implementation of the service via JPA
 */
//@Transactional(readOnly = true)
public class CouponDistributionServiceImpl implements CouponDistributionService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CouponDistributionServiceImpl.class);

    @Inject
    private DistributionRuleRepository distributionRuleRepository;

    @Inject
    private DistributionJobScheduler distributionJobScheduler;

    @Inject
    private DistributionRuleConverter converter;

    @Override
    public DistributionRule createDistributionRule(DistributionRule rule) throws DistributionRuleExistsException, NoSuchDistributionRuleException {
        rule.setStatus(DistributionStatus.NEW);
        DistributionRuleDocument document = distributionRuleRepository.save(converter.convert(rule));

        rule = converter.convert(document);

        try {
            distributionJobScheduler.scheduleDistributionJob(rule);
        } catch (SchedulerException e) {
            e.printStackTrace();
        }
        return rule;
    }

    @Override
    public DistributionRule updateDistributionRule(DistributionRule rule) {
        DistributionRuleDocument document = distributionRuleRepository.save(converter.convert(rule));
        rule = converter.convert(document);
        return rule;
    }

    @Override
    public DistributionRule getDistributionRule(DistributionRuleId id) throws NoSuchDistributionRuleException {
        return null;
    }
}
