package com.tillster.platform.service.datamodel;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * An {@link IdentifiableEntity} is a {@link MappedSuperclass} for entities that have a
 * {@link Long} unique ID column.
 *
 * @author Mirko Raner (Tillster, Inc.)
 */
@MappedSuperclass
public abstract class IdentifiableEntity {

    private Long id;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", length = 20, nullable = false, unique = true)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
