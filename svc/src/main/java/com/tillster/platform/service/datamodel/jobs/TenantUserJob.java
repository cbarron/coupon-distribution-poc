package com.tillster.platform.service.datamodel.jobs;

import com.tillster.platform.service.CouponDistributionService;
import com.tillster.platform.service.CouponDistributionServiceImpl;
import com.tillster.platform.service.datamodel.DistributionRuleRepository;
import com.tillster.platform.service.datamodel.UserEntity;
import com.tillster.platform.service.datamodel.UserEntityRepository;
import com.tillster.platform.service.model.DistributionRule;
import com.tillster.platform.service.model.DistributionStatus;
import com.tillster.service.coupon.wallet.model.Coupon;
import java.time.Instant;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import javax.inject.Inject;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;
import org.springframework.web.util.UriComponentsBuilder;

@Component
public class TenantUserJob implements Job {

    @Value("${coupon.wallet.endpoint}")
    private String couponWalletEndpoint;

    @Inject
    private UserEntityRepository userEntityRepository;

    @Inject
    private CouponDistributionService couponDistributionService;

    @Inject
    private RestTemplate restTemplate;

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {

        SpringBeanAutowiringSupport.processInjectionBasedOnCurrentContext(this);

        DistributionRule rule = (DistributionRule) jobExecutionContext.getJobDetail().getJobDataMap().get(DistributionRule.class.toString());

        rule.setStatus(DistributionStatus.RUNNING);
        couponDistributionService.updateDistributionRule(rule);

        System.out.println("Executing " + this.getClass().toString() + ", id: " + rule.getId().getDistributionRuleId());

        List<UserEntity> users;
        if (CollectionUtils.isEmpty(rule.getUserIds())) {
            users = userEntityRepository.findByTenantName(rule.getTenant());
        } else {
            users = userEntityRepository.findByIdIn(rule.getUserIds());
        }

        for (UserEntity userEntity : users) {
            rule.getCouponIds()
                    .stream()
                    .forEach(
                            couponId -> {
                                System.out.println("adding coupons to user: " + userEntity.getId());

                                Instant instant = Instant.now();
                                instant = instant.plus(30, ChronoUnit.DAYS);

                                Coupon walletCoupon = new Coupon();
                                walletCoupon.setCouponId(couponId);
                                walletCoupon.setName(rule.getName());
                                walletCoupon.setExpirationDate(Date.from(instant));

                                HttpHeaders headers = new HttpHeaders();
                                headers.setContentType(MediaType.APPLICATION_JSON);
                                headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
                                headers.set("ticket", "0a39e0ca-d61b-41dc-a515-9ffd75f09a4c");
                                headers.set("tenant", rule.getTenant());

                                UriComponentsBuilder uriComponentsBuilder =
                                        UriComponentsBuilder.fromHttpUrl(couponWalletEndpoint.replace("{userId}", userEntity.getId()));

                                HttpEntity<Coupon> httpEntity = new HttpEntity<>(walletCoupon, headers);
                                try {
                                    restTemplate.postForEntity(uriComponentsBuilder.toUriString(), httpEntity, Coupon.class);
                                } catch (Exception e) {
                                    System.out.println(e);
                                }
                            });
        }

        rule.setStatus(DistributionStatus.PROCESSED);
        couponDistributionService.updateDistributionRule(rule);
    }
}
