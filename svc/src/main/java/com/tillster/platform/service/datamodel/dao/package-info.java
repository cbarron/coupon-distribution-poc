/**
 * This package is used when the quantity of data prevents the use of JPA.
 *
 * Use this packe for bulk inserts and deletes via Spring JDBC
 */
package com.tillster.platform.service.datamodel.dao;
