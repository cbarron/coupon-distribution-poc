/**
 * This package holds the JPA objects
 *
 * These objects are used for reading and writing to the DB
 *
 */
package com.tillster.platform.service.datamodel;
