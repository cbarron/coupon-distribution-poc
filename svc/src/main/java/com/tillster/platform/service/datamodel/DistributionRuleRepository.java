package com.tillster.platform.service.datamodel;

import com.tillster.platform.service.model.DistributionRule;
import com.tillster.platform.service.model.EventNotificationType;
import java.util.List;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface DistributionRuleRepository extends MongoRepository<DistributionRuleDocument, String> {

    List<DistributionRuleDocument> findByTenantAndEventTrigger(final String tenant, final EventNotificationType eventTrigger);
}
